﻿using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace KySo
{
    public partial class QuanLyChuKy : Form
    {
        public string currentSubjectToken;
        public PublicKey currentPublicKey;
        public string currentLocation;
        public string currentImg;
        public string currentImgThumb;
        public string currentSerialNumber;

        public string currentSelectedMau;
        Form1 ownerForm = null;


        public QuanLyChuKy(Form1 ownerForm)
        {
            InitializeComponent();
            this.ownerForm = ownerForm;
            var lsChuKy = LoadAllChuKySo();
            var count_chu_ky = 0;
            var dts = new List<ChuKySoInfo>();
            foreach (var item in lsChuKy)
            {
                var i = new ChuKySoInfo();
                var af = LoadChuKySo(count_chu_ky);
                if (af != null)
                {
                    i.SubjectChuKy = af.Subject;
                    i.CertInfo = af;
                    comboBox1.Items.Add(i);
                    //dts.Add(i);
                    count_chu_ky++;
                }
            }
            string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            
            currentLocation = Path.GetDirectoryName(path);
            //comboBox1.Items.AddRange(dts);
            //comboBox1.DisplayMember = "Signature";
            //comboBox1.DataSource = dts;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var r = new ChuKySoInfo();
            r = (ChuKySoInfo)comboBox1.SelectedItem;
            if(r != null)
            {
                currentSubjectToken = r.CertInfo.Subject;
                currentPublicKey = r.CertInfo.PublicKey;
                currentSerialNumber= r.CertInfo.SerialNumber;
                
            }
            var duong_dan_db = Path.Combine(currentLocation, "Db").Replace("file:\\", "");
            if (!Directory.Exists(duong_dan_db))
            {
                Directory.CreateDirectory(duong_dan_db);
            }
            try
            {
                var ls_mau = BinaryRage.DB.Get<List<SaveMauChuKy>>(currentSubjectToken, duong_dan_db);
                if (ls_mau != null)
                {
                    foreach (var item in ls_mau)
                    {
                        var d = new MauChuKySoInfo();
                        d.Ten = item.Ten;
                        d.UrlImages = item.Full_Image + ";" + item.Thumb_Image;
                        comboBox2.Items.Add(d);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //throw;
            }
            
        }

        private X509Certificate2 LoadChuKySo(int index)
        {
            X509CertificateParser cp = new X509CertificateParser();

            //Get Sertifiacte
            X509Certificate2 certClient = new X509Certificate2();
            X509Store st = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            st.Open(OpenFlags.MaxAllowed);
            //X509FindType.FindByTimeValid,DateTime.Now,false
            X509Certificate2Collection collection = st.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
            if (collection.Count > 0)
            {
                certClient = collection[index];
            }

            st.Close();
            return certClient;
        }
        private X509Certificate2Collection LoadAllChuKySo()
        {
            X509CertificateParser cp = new X509CertificateParser();

            //Get Sertifiacte
            X509Certificate2 certClient = new X509Certificate2();
            X509Store st = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            st.Open(OpenFlags.MaxAllowed);
            //X509FindType.FindByTimeValid,DateTime.Now,false
            X509Certificate2Collection collection = st.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
            //if (collection.Count > 0)
            //{
            //    certClient = collection[1];
            //}

            st.Close();
            return collection;
        }

        private void btn_tai_anh_len_Click(object sender, EventArgs e)
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Png Image|*.png";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {


                var path_file = Path.GetFullPath(saveFileDialog1.FileName);

                //var file_name = saveFileDialog1.FileName;


                //var file_stream = saveFileDialog1.OpenFile();
                var duong_dan_luu = Path.Combine(currentLocation, "Upload_Image").Replace("file:\\", "");
                var duong_dan_thumb = Path.Combine(currentLocation, "Thumb_Image").Replace("file:\\", "");
                if (!Directory.Exists(duong_dan_luu))
                {
                    Directory.CreateDirectory(duong_dan_luu);
                }
                if (!Directory.Exists(duong_dan_thumb))
                {
                    Directory.CreateDirectory(duong_dan_thumb);
                }
                var d = DateTime.Now;
                
                var file_luu = Path.Combine(duong_dan_luu, string.Format("{0}_{1}",d.Ticks, Path.GetFileName(saveFileDialog1.FileName)));
                var file_thumb = Path.Combine(duong_dan_thumb, string.Format("{0}_{1}", d.Ticks, Path.GetFileName(saveFileDialog1.FileName)));

                //var reader = new StreamReader(file_stream);
                saveFileDialog1.Reset();
                

                string sourceFile = System.IO.Path.Combine(path_file);
                string destFile = System.IO.Path.Combine(duong_dan_luu);


                //System.IO.Directory.CreateDirectory(duong_dan_luu);
                System.IO.File.Copy(sourceFile, file_luu, true);
                System.Drawing.Image img = System.Drawing.Image.FromFile(sourceFile);
                pictureBox1.Image = img;
                var size = new Size(100, 100);

                img = ResizeImage(img, size);
                img.Save(file_thumb);
                currentImg = file_luu;
                currentImgThumb = file_thumb;
                
            }
        }

        
        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                var originalWidth = image.Width;
                var originalHeight = image.Height;
                var percentWidth = size.Width / (float)originalWidth;
                var percentHeight = size.Height / (float)originalHeight;
                var percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (var graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.SmoothingMode = SmoothingMode.HighQuality;
                graphicsHandle.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ownerForm.PassValue(currentImg, currentSubjectToken,  currentSerialNumber);
            
            var duong_dan_db = Path.Combine(currentLocation, "Db").Replace("file:\\", "");
            if (!Directory.Exists(duong_dan_db))
            {
                Directory.CreateDirectory(duong_dan_db);
            }
            try
            {
                var toAdd = BinaryRage.DB.Get<List<SaveMauChuKy>>(currentSubjectToken, duong_dan_db) ?? new List<SaveMauChuKy>();
                var add = new SaveMauChuKy();
                add.SubjectChuKy = currentSubjectToken;
                add.Full_Image = currentImg;
                add.Thumb_Image = currentImgThumb;
                var ten = "";
                if (!string.IsNullOrEmpty(currentSelectedMau))
                {
                    add.Ten = currentSelectedMau;
                    if (toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault() != null)
                    {
                        var r = toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault();
                        ten = r.Ten;
                        toAdd.Remove(r);
                    }
                }

                else if (toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault() != null)
                {
                    var r = toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault();
                    ten = r.Ten;
                    toAdd.Remove(r);
                }
                else if (!string.IsNullOrEmpty(textBox1.Text))
                {
                    ten = textBox1.Text;
                }
                else
                {
                    add.Ten = "Mẫu chữ kí số_" + DateTime.Now.Ticks;
                }
                
                toAdd.Add(add);
                BinaryRage.DB.Insert(currentSubjectToken, toAdd, duong_dan_db);
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var toAdd = new List<SaveMauChuKy>();
                var add = new SaveMauChuKy();
                add.SubjectChuKy = currentSubjectToken;
                add.Full_Image = currentImg;
                add.Thumb_Image = currentImgThumb;
                add.Ten = string.IsNullOrEmpty(textBox1.Text) ? "Mẫu chữ kí số_" + DateTime.Now.Ticks : textBox1.Text;
                toAdd.Add(add);
                BinaryRage.DB.Insert(currentSubjectToken, toAdd, duong_dan_db);
                this.Close();
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.ownerForm.PassValue(currentImgThumb, currentSubjectToken, currentSerialNumber);
            var duong_dan_db = Path.Combine(currentLocation, "Db").Replace("file:\\", "");
            if (!Directory.Exists(duong_dan_db))
            {
                Directory.CreateDirectory(duong_dan_db);
            }
            try
            {
                var toAdd = BinaryRage.DB.Get<List<SaveMauChuKy>>(currentSubjectToken, duong_dan_db) ?? new List<SaveMauChuKy>();
                var add = new SaveMauChuKy();
                add.SubjectChuKy = currentSubjectToken;
                add.Full_Image = currentImg;
                add.Thumb_Image = currentImgThumb;
                var ten = "";
                if (!string.IsNullOrEmpty(currentSelectedMau))
                {
                    add.Ten = currentSelectedMau;
                    if (toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault() != null)
                    {
                        var r = toAdd.Where(x => x.Ten.Equals(currentSelectedMau)).FirstOrDefault();
                        ten = r.Ten;
                        toAdd.Remove(r);
                    }
                }
                

                else if (!string.IsNullOrEmpty(textBox1.Text))
                {
                    ten = textBox1.Text;
                }
                else
                {
                    add.Ten = "Mẫu chữ kí số_" + DateTime.Now.Ticks;
                }
                //add.Ten = "Mẫu chữ kí số_" + DateTime.Now.Ticks;
                
                toAdd.Add(add);
                BinaryRage.DB.Insert(currentSubjectToken, toAdd, duong_dan_db);
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                var toAdd = new List<SaveMauChuKy>();
                var add = new SaveMauChuKy();
                add.SubjectChuKy = currentSubjectToken;
                add.Full_Image = currentImg;
                add.Thumb_Image = currentImgThumb;
                add.Ten = string.IsNullOrEmpty(textBox1.Text)  ? "Mẫu chữ kí số_" + DateTime.Now.Ticks : textBox1.Text;
                toAdd.Add(add);
                BinaryRage.DB.Insert(currentSubjectToken, toAdd, duong_dan_db);
                this.Close();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var r = new MauChuKySoInfo();
            r = (MauChuKySoInfo)comboBox2.SelectedItem;
            if(r != null)
            {
                var i = r.UrlImages.Split(';');
                if (i.Count() >= 1)
                    currentImg = i[0];
                if (i.Count() >= 2)
                    currentImgThumb = i[1];
                pictureBox1.Image = System.Drawing.Image.FromFile(currentImg);
                currentSelectedMau = r.Ten;
                textBox1.Text = "";
                textBox1.Visible = false;
                label3.Visible = false;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
    public class ChuKySoInfo
    {
        public string SubjectChuKy { get; set; }
        public X509Certificate2 CertInfo { get; set; }
        public override string ToString()
        {
            return SubjectChuKy;
        }
    }

    public class MauChuKySoInfo
    {
        public string Ten { get; set; }
        public string UrlImages { get; set; }
        public override string ToString()
        {
            return Ten;
        }
    }
    
    
    [Serializable]

    public class SaveMauChuKy
    {
        public string SubjectChuKy { get; set; }
        public string Full_Image { get; set; }
        public string Thumb_Image { get; set; }
        public string Ten { get; set; }
    }

}
