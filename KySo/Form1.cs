﻿using Ghostscript.NET;
using iTextSharp.text.pdf;
using Org.BouncyCastle.X509;
//using iText.Kernel.Pdf;

//using iTextSharp.text.pdf;
//using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Org.BouncyCastle.Security;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;
using iText.Signatures;
using iTextSharp.text.pdf.security;
using Org.BouncyCastle.Crypto;

//using iTextSharp.text;

namespace KySo
{
    public partial class Form1 : Form
    {
        private int currentPage = -1;
        private string currentFile = "";
        private int totalPage = 0;
        private List<UsedImage> UsedImages = new List<UsedImage>();
        private Cursor _default = System.Windows.Forms.Cursor.Current;
        private string currentImage;
        private string currentSubjectToken;
        private string currentSerialNumber;
        private int currentFileWidth;
        private int currentFileHeight;


        //set up for drawing
        Rectangle rect = new Rectangle(125, 125, 50, 50);
        bool isMouseDown = false;
        bool drawing;
        Point startPos;
        Point currentPos;
        Point currentPoint;
        List<Rectangle> rectangles = new List<Rectangle>();
        //setup child from

        private Rectangle getRectangle()
        {
            return new Rectangle(
                Math.Min(startPos.X, currentPos.X),
                Math.Min(startPos.Y, currentPos.Y),
                Math.Abs(startPos.X - currentPos.X),
                Math.Abs(startPos.Y - currentPos.Y));
        }
        public Form1()
        {
            InitializeComponent();

            prevewPage.Visible = false;
            nextPage.Visible = false;

        }
        public void PassValue(string strValue, string subjectToken, string serialNumber)
        {
            currentImage = strValue;
            currentSubjectToken = subjectToken;
            currentSerialNumber = serialNumber;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            int size = -1;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    
                    //PdfDocument pdfDoc = new PdfDocument(new PdfReader(file), new PdfWriter(dest));
                    PdfReader reader = new PdfReader(openFileDialog1.OpenFile());
                    var total = reader.NumberOfPages;
                    for (int i = 0; i < total; i++)
                    {
                        var r = new UsedImage();
                        r.IsOriginal = true;
                        r.PageNumber = i;
                        UsedImages.Add(r);
                    }
                    if (total > 0)
                    {
                        currentFile = file;
                        currentPage = 0;
                        totalPage = total;
                        ShowPageOption();
                        LoadImage(currentFile, currentPage);
                    }



                }
                catch (IOException)
                {
                }
            }
            //Console.WriteLine(size); // <-- Shows file size in debugging mode.
            //Console.WriteLine(result); // <-- For debugging use.
        }

        private void LoadImage(string InputPDFFile, int PageNumber)
        {

            string outImageName = Path.GetFileNameWithoutExtension(InputPDFFile);
            outImageName = outImageName + "_" + PageNumber.ToString() + "_.png";
            var rootPath = System.IO.Directory.GetCurrentDirectory();
            var savePath = Path.Combine(rootPath, "Output", outImageName);

            try
            {
                using (var document = PdfiumViewer.PdfDocument.Load(InputPDFFile))
                {


                    var aft = UsedImages.Where(r => r.PageNumber == PageNumber).FirstOrDefault();


                    if (aft != null)
                    {
                        var image = aft.PageImage;
                        if (image == null)
                        {
                            PdfReader reader = new PdfReader(InputPDFFile);
                            var s = reader.GetPageN(PageNumber + 1);
                            
                            var size = reader.GetPageSize(s);
                            var x = (int)Math.Round(size.Width,0);
                            var y = (int)Math.Round(size.Height,0);
                            currentFileHeight = x;
                            currentFileWidth = y;
                            image = document.Render(PageNumber, x,y,720,720, false);
                        }
                        Console.WriteLine(pictureBox2.Size);
                        pictureBox2.Image = image;
                        //pictureBox2.Width = image.Width-5;
                        //pictureBox2.Height = panel1.Height;
                        pictureBox2.SizeMode = PictureBoxSizeMode.AutoSize;

                        panel1.HorizontalScroll.Maximum = 0;
                        panel1.AutoScroll = false;
                        panel1.VerticalScroll.Visible = false;
                        panel1.AutoScroll = true;
                        panel1.Width = pictureBox2.Width;

                        pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
                        pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
                        pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
                        pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);

                    }
                }
            }
            catch (Exception ex)
            {
                // handle exception here;
            }



        }
        private void ShowPageOption()
        {
            if (currentPage < 0)
            {
                prevewPage.Visible = false;
                nextPage.Visible = false;
            }
            if (currentPage == 0)
            {
                prevewPage.Visible = false;
                nextPage.Visible = true;
            }
            if (currentPage > 0 && currentPage < totalPage - 1)
            {
                prevewPage.Visible = true;
                nextPage.Visible = true;
            }
            if (currentPage == totalPage - 1)
            {
                prevewPage.Visible = true;
                nextPage.Visible = false;
            }
            groupBox1.Text = "Control Page - Page " + (currentPage + 1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void nextPage_Click(object sender, EventArgs e)
        {
            currentPage++;
            ShowPageOption();
            LoadImage(currentFile, currentPage);
        }

        private void prevewPage_Click(object sender, EventArgs e)
        {
            currentPage--;
            ShowPageOption();
            LoadImage(currentFile, currentPage);
        }

        //set up for paint
        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            //this.Refresh();
            var aft = UsedImages.Where(r => r.PageNumber == currentPage).FirstOrDefault();
            if (aft != null)
            {
                var r = aft.ListRectangles;
                foreach (var item in r)
                {
                    if (!string.IsNullOrEmpty(item.ImageUrl))
                    {
                        using (Font font1 = new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point))
                        {
                            var poi = new Point();
                            poi.X = item.GetRectangle.X;
                            poi.Y = item.GetRectangle.Y;
                            System.Drawing.Image img = System.Drawing.Image.FromFile(item.ImageUrl);
                            e.Graphics.DrawImage(img, poi);
                        }

                    }
                }
            }
            //if (rectangles.Count > 0) e.Graphics.DrawRectangles(Pens.Black, rectangles.ToArray());
            if (drawing)
            {
                var sign = LoadChuKySo();
                if (!string.IsNullOrEmpty(currentImage))
                {
                    using (Font font1 = new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Point))
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromFile(currentImage);

                        e.Graphics.DrawImage(img, currentPoint);
                    }
                }

            }
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            currentPos = startPos = e.Location;
            drawing = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Cross;
            MouseEventArgs c = (MouseEventArgs)e;
            currentPoint = c.Location;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (!string.IsNullOrEmpty(currentImage))
            {
                Bitmap bmp = new Bitmap(currentImage);
                this.Cursor = new Cursor(bmp.GetHicon());
            }

        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (drawing)
            {
                drawing = false;
                var rc = getRectangle();
                if (rc.Width > 0 && rc.Height > 0) rectangles.Add(rc);
                pictureBox2.Invalidate();
                var aft = UsedImages.Where(r => r.PageNumber == currentPage).FirstOrDefault();
                if (aft != null)
                {
                    var t = new MixedRectangle();
                    t.GetRectangle = rc;
                    t.TextInside = LoadChuKySo().Subject;
                    t.SubjectKey = currentSubjectToken;
                    t.ImageUrl = currentImage;
                    aft.ListRectangles.Add(t);
                }
            }

            System.Windows.Forms.Cursor.Current = _default;
            //open form 2

        }
        private X509Certificate2 LoadChuKySo()
        {
            X509CertificateParser cp = new X509CertificateParser();

            //Get Sertifiacte
            X509Certificate2 certClient = new X509Certificate2();
            X509Store st = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            st.Open(OpenFlags.MaxAllowed);
            //X509FindType.FindByTimeValid,DateTime.Now,false
            X509Certificate2Collection collection = st.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
            if (collection.Count > 0)
            {
                certClient = collection[1];
            }

            st.Close();
            return certClient;
        }
        private X509Certificate2 LoadChuKySo_v1(string subject_key)
        {
            X509CertificateParser cp = new X509CertificateParser();

            //Get Sertifiacte
            X509Certificate2 certClient = new X509Certificate2();
            X509Store st = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            st.Open(OpenFlags.MaxAllowed);
            //X509FindType.FindByTimeValid,DateTime.Now,false
            X509Certificate2Collection collection = st.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    if (item.Subject.Equals(subject_key))
                    {
                        certClient = item;
                    }
                }
            }
            st.Close();
            return certClient;
        }

        private void btn_action_ky_Click(object sender, EventArgs e)
        {
            var cau_hinh_form = new QuanLyChuKy(this);
            cau_hinh_form.Show();
        }

        private void btn_luu_file_Click(object sender, EventArgs e)
        {
            //Tao ra file o day
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "PDF|*.pdf";
            save.Title = "Save file";
            save.ShowDialog();
            if (save.FileName != "")
            {
                try
                {

                    var width = 0;
                    var height = 0;
                    var lsImg = new List<Bitmap>();
                    width = pictureBox2.ClientSize.Width;
                    height = pictureBox2.ClientSize.Height;
                    foreach (var item in UsedImages)
                    {

                        currentPage = item.PageNumber;
                        drawing = false;
                        LoadImage(currentFile, item.PageNumber);
                        pictureBox2.Invalidate();
                        var img = new Bitmap(pictureBox2.ClientSize.Width, pictureBox2.ClientSize.Height);
                        pictureBox2.DrawToBitmap(img, pictureBox2.ClientRectangle);
                        Console.WriteLine(img);
                        lsImg.Add(img);

                    }
                    iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(width,height);
                    //iTextSharp.text.Document doc1 = new iTextSharp.text.Document();
                    //iTextSharp.text.Document doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
                    iTextSharp.text.Document doc = new iTextSharp.text.Document(rec);
                    PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(save.FileName, FileMode.Create));
                    doc.Open();
                    doc.AddHeader("Filegenerated", DateTime.Now.ToString("dd/MM/yyyy"));
                    var rendered_page = 0;
                    foreach (var item in lsImg)
                    {
                        
                        iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(item, System.Drawing.Imaging.ImageFormat.Bmp);
                        pic.SetDpi(1000, 1000);
                        //if (pic.Height > pic.Width)
                        //{
                        //    //Maximum height is 800 pixels.
                        //    float percentage = 0.0f;
                        //    percentage = 700 / pic.Height;
                        //    pic.ScalePercent(percentage * 100);
                        //}
                        //else
                        //{
                        //    //Maximum width is 600 pixels.
                        //    float percentage = 0.0f;
                        //    percentage = 540 / pic.Width;
                        //    pic.ScalePercent(percentage * 100);
                        //}
                        //pic.Border = iTextSharp.text.Rectangle.BOX;
                        //pic.BorderColor = iTextSharp.text.BaseColor.BLACK;
                        //pic.BorderWidth = 3f;
                        doc.Add(pic);
                        doc.NewPage();
                    }
                    var ls_subject = new List<string>();
                    for (int i = 0;  i < UsedImages.Count; i++)
                      {
                        var ls_rectangle = UsedImages[i].ListRectangles;
                        if (ls_rectangle != null)
                        {
                            foreach (var ck in ls_rectangle)
                            {
                                if (ls_subject.IndexOf(ck.SubjectKey) < 0)
                                {
                                    ls_subject.Add(ck.SubjectKey);
                                }
                            }
                        }
                    }
                    doc.Close();
                    var name_temp = save.FileName;
                    wri.Close();
                    save.Reset();
                    //Bat dau ki



                    try
                    {
                        var _byte = System.IO.File.ReadAllBytes(name_temp);
                        var reader = new PdfReader(_byte);
                        using (FileStream fileStreamSigPdf = new FileStream(name_temp, FileMode.Append, FileAccess.Write))
                        {
                            //fileStreamSigPdf.Position = 0;
                           
                            var stamper = PdfStamper.CreateSignature(reader, fileStreamSigPdf, '\0');
                            
                            var appearance = stamper.SignatureAppearance;

                            appearance.Reason = "Ky so";
                            //appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(0, 00, 200, 100), 1, "Signature");
                            

                            foreach(var item in UsedImages)
                            {
                                var page = item.PageNumber + 1;
                                var recs = item.ListRectangles;
                                
                                if(recs != null)
                                {
                                    var count = 1;
                                    foreach (var jtem in recs)
                                    {
                                        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(jtem.GetRectangle.X, jtem.GetRectangle.Y,200,100), page, "singinture");
                                        count++;
                                    }
                                }
                            }
                            appearance.SignatureRenderingMode = iTextSharp.text.pdf.PdfSignatureAppearance.RenderingMode.GRAPHIC;
                            foreach (var item in ls_subject)
                            {
                                // Acquire certificate chain
                                X509Certificate2 certClient = null;
                                X509Store st = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                                st.Open(OpenFlags.MaxAllowed);
                                X509Certificate2Collection collection = st.Certificates;
                                for (int i = 0; i < collection.Count; i++)
                                {
                                    foreach (X509Certificate2 c in collection)
                                    {
                                        certClient = c;
                                        
                                        if (collection[i].Subject.Equals(ls_subject))
                                        {
                                            certClient = collection[i];
                                        }
                                    }
                                }
                                st.Close();
                                //Get Certificate Chain    
                                IList<X509Certificate> chain = new List<X509Certificate>();
                                X509Chain x509Chain = new X509Chain();
                                x509Chain.Build(certClient);
                                foreach (X509ChainElement x509ChainElement in x509Chain.ChainElements)
                                {
                                    chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
                                }
                                iTextSharp.text.pdf.security.IExternalSignature externalSignature = new X509Certificate2Signature(certClient, "SHA-256");
                                MakeSignature.SignDetached(appearance, externalSignature, chain, null, null, null, 0, CryptoStandard.CMS);






                                //var certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                                //certStore.Open(OpenFlags.MaxAllowed);


                                //X509CertificateCollection certCollection =
                                //    certStore.Certificates.Find(X509FindType.FindBySerialNumber,
                                //    currentSerialNumber, false);
                                
                                //System.Security.Cryptography.X509Certificates.X509Certificate cert = certCollection[0];
                                //var converted = Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(cert);
                                //IList<X509Certificate> chain = new List<X509Certificate>();
                                //X509Chain x509Chain = new X509Chain();
                                //x509Chain.Build(certClient);
                                //var chans = new List<Org.BouncyCastle.X509.X509Certificate>();
                                //chans.Add(converted);
                                ////cert = certCollection[0];
                                //var cert1 = LoadChuKySo_v1(item);
                                //var ex = new X509Certificate2Signature(cert1, "SHA-256");
                                ////var pks = Org.BouncyCastle.Security.DotNetUtilities.GetKeyPair(converted.GEt).Private;
                                ////iTextSharp.text.pdf.security.IExternalSignature pk = new iTextSharp.text.pdf.security.PrivateKeySignature(pks, "SHA-256");

                                //MakeSignature.SignDetached(appearance, ex, chans, null, null, null, 0, CryptoStandard.CMS);
                            }


                            fileStreamSigPdf.Close();
                            reader.Close();
                            stamper.Close();
                        }






                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        throw;

                    }



                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                }

                //Save file vao day;

            }
        }

        private void pdfViewer1_Load(object sender, EventArgs e)
        {

        }

        private void pdfViewerMousedown(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Hello Na");
        }
    }

    public class UsedImage
    {
        public int PageNumber { get; set; } = 0;
        public Image PageImage { get; set; } = null;
        public bool IsOriginal { get; set; } = true;
        public List<MixedRectangle> ListRectangles { get; set; } = new List<MixedRectangle>();
    }

    public class MixedRectangle
    {
        public Rectangle GetRectangle { get; set; }
        public string TextInside { get; set; }
        public string SubjectKey { get; set; }
        public string ImageUrl { get; set; }
    }
}
