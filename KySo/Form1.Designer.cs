﻿
namespace KySo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nextPage = new System.Windows.Forms.Button();
            this.prevewPage = new System.Windows.Forms.Button();
            this.btn_action_ky = new System.Windows.Forms.Button();
            this.btn_luu_file = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 15);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(324, 73);
            this.button1.TabIndex = 0;
            this.button1.Text = "Open PDF File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "PDF Files|*.pdf";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nextPage);
            this.groupBox1.Controls.Add(this.prevewPage);
            this.groupBox1.Location = new System.Drawing.Point(16, 113);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(324, 125);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Control Page";
            // 
            // nextPage
            // 
            this.nextPage.Location = new System.Drawing.Point(164, 62);
            this.nextPage.Margin = new System.Windows.Forms.Padding(4);
            this.nextPage.Name = "nextPage";
            this.nextPage.Size = new System.Drawing.Size(152, 56);
            this.nextPage.TabIndex = 4;
            this.nextPage.Text = "Next";
            this.nextPage.UseVisualStyleBackColor = true;
            this.nextPage.Click += new System.EventHandler(this.nextPage_Click);
            // 
            // prevewPage
            // 
            this.prevewPage.Location = new System.Drawing.Point(8, 62);
            this.prevewPage.Margin = new System.Windows.Forms.Padding(4);
            this.prevewPage.Name = "prevewPage";
            this.prevewPage.Size = new System.Drawing.Size(136, 56);
            this.prevewPage.TabIndex = 4;
            this.prevewPage.Text = "Preview";
            this.prevewPage.UseVisualStyleBackColor = true;
            this.prevewPage.Click += new System.EventHandler(this.prevewPage_Click);
            // 
            // btn_action_ky
            // 
            this.btn_action_ky.Location = new System.Drawing.Point(24, 260);
            this.btn_action_ky.Margin = new System.Windows.Forms.Padding(4);
            this.btn_action_ky.Name = "btn_action_ky";
            this.btn_action_ky.Size = new System.Drawing.Size(308, 69);
            this.btn_action_ky.TabIndex = 4;
            this.btn_action_ky.Text = "Ký số";
            this.btn_action_ky.UseVisualStyleBackColor = true;
            this.btn_action_ky.Click += new System.EventHandler(this.btn_action_ky_Click);
            // 
            // btn_luu_file
            // 
            this.btn_luu_file.Location = new System.Drawing.Point(24, 362);
            this.btn_luu_file.Margin = new System.Windows.Forms.Padding(6);
            this.btn_luu_file.Name = "btn_luu_file";
            this.btn_luu_file.Size = new System.Drawing.Size(308, 67);
            this.btn_luu_file.TabIndex = 6;
            this.btn_luu_file.Text = "Lưu File";
            this.btn_luu_file.UseVisualStyleBackColor = true;
            this.btn_luu_file.Click += new System.EventHandler(this.btn_luu_file_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(352, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 838);
            this.panel1.TabIndex = 7;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(896, 833);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pdfViewer1
            // 
           
            
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2549, 869);
            
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_luu_file);
            this.Controls.Add(this.btn_action_ky);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button nextPage;
        private System.Windows.Forms.Button prevewPage;
        private System.Windows.Forms.Button btn_action_ky;
        private System.Windows.Forms.Button btn_luu_file;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        
    }
}

